# Contributing to GitNex

Please take a few minutes to read this document to make the process of contribution more easy and healthy for all involved.

## Pull Requests
Patches, enhancements, features are always welcome. The PR should focus on the scope of work and avoid many unnecessary commits. Please provide as much detail and context as possible to explain the work submitted.

Please ask if you are not sure about the scope of work to be submitted to avoid waste of time spent on the work.

**Code Standards**
Please follow the code standards, this will help other developers to understand your code too.  
It also helps maintaining the code afterwards.  
It is documented in the Wiki: [Code-Standards](https://gitea.com/gitnex/GitNex/wiki/Code-Standards)

**How to submit a PR**  
Fork this repository. Pull the forked repository from your namespace to your local machine. Create new branch and work on the bug/feature/enhancement you would like to submit. Push it to your forked version. From there create Pull Request(PR) against **master** branch.

**IMPORTANT:** By submitting PR, you agree to allow GitNex to license your work under the same license as that used by GitNex.

## Issues and Reports
*1st of please be polite and gentle while commenting or creating new issue to maintain a healthy environment.*

Before creating an issue please take a moment and search the repository issues(open/closed) to avoid duplicate issues either it's a bug or feature.

In case you want to submit a bug report, please provide as much details as possible to better debug the problem. The important part is how to reproduce the bug and steps to reproduce are appreciated.

**Note:** Please contact the project directly via email(gitnex@swatian.com) if have to share sensitive and security related details.
